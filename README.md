# To use it #

Get started git clone `git clone git@bitbucket.org:koompi/koompi-logo-plymouth.git` 
to `/usr/share/plymouth/themes/` for Debian/Ubuntu based 16.04 and newer.

## Type a few command ##

Check if the files install into the theme

```shell
sudo update-alternatives --config default.plymouth
```

Enter `sudo password:`

Install the plymouth we just made;

```shell
sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/koompi-logo-plymouth/koompi-logo.plymouth 100
```

Then select the plymouth

```shell
sudo update-alternatives --config default.plymouth
```

```shell
There are 5 choices for the alternative default.plymouth (providing /usr/share/plymouth/themes/default.plymouth).

  Selection    Path                                                                               Priority   Status
------------------------------------------------------------
  0            /usr/share/plymouth/themes/ubuntu-logo/ubuntu-logo.plymouth           150       auto mode
  1            /usr/share/plymouth/themes/koompi-logo/koompi-logo-scale-2.plymouth                 100       manual mode
* 2            /usr/share/plymouth/themes/koompi-logo/koompi-logo.plymouth                         100       manual mode
  3            /usr/share/plymouth/themes/spinner/spinner.plymouth                                 70        manual mode

```

Then follow instruction and choose the plymouth.
    
## Enable it 

```shell
sudo update-initramfs -u
```

Reboot